# LeBonCoin Technical Test (Le Bon Test)

## Introduction
This application is a my technical test for the company Le Bon Coin

- Code structuring as per clean Architecture
- Using MVVM Pattern as per Google's recommendation
- Android Architecture Components (LiveData, ViewModel, Navigation)
- Kotlin features ( Extension functions, sealed class and Coroutines)

## App Overview
 The app features a single screen

- List screen displaying articles
- Rotation screen

Navigation between the screens has been done using the Jetpack Navigation library.
A workaround has been implemmented to show the images from the placeholder website.

## Libraries used
- [Kotlin](https://kotlinlang.org/) first
- [Coroutines Flow API](https://kotlinlang.org/docs/reference/coroutines/flow.html)
- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture)
- [Retrofit](https://square.github.io/retrofit/) for networking
- [Room](https://developer.android.com/training/data-storage/room) for data storage
- [Glide](https://github.com/bumptech/glide) for images
- [Hilt](https://dagger.dev/hilt/) for dependency injection
- [Android KTX](https://developer.android.com/kotlin/ktx) features
- [Junit5](https://github.com/mannodermaus/android-junit5) for tests
- [Mockserver](https://github.com/square/okhttp/tree/master/mockwebserver) for networking mock


### Scope for Improvements
 The app can be further improved with the addition of the following features

- Add Skeleton
- Scroll to refresh
- Pagination in Recyclerview