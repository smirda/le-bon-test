package com.smirda.lebontest.domain.usecase

import com.google.gson.GsonBuilder
import com.smirda.lebontest.data.repository.ArticleRepositoryImp
import com.smirda.lebontest.data.source.local.AppDatabaseFake
import com.smirda.lebontest.data.source.local.ArticleDao
import com.smirda.lebontest.data.source.local.ArticleDaoFake
import com.smirda.lebontest.data.source.remote.ArticleRemoteSource
import com.smirda.lebontest.data.source.remote.MockWebServerResponses
import com.smirda.lebontest.data.source.remote.service.ArticleService
import com.smirda.lebontest.domain.common.Resource
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.HttpUrl
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

class FetchAndSaveArticlesUseCaseTest {

    private val appDatabase = AppDatabaseFake()
    private lateinit var mockWebServer: MockWebServer
    private lateinit var baseUrl: HttpUrl

    private lateinit var fetchAndSaveArticlesUseCase: FetchAndSaveArticlesUseCase

    private lateinit var articleService: ArticleService

    private lateinit var articleRepositoryImp: ArticleRepositoryImp
    private lateinit var articleRemoteSource: ArticleRemoteSource
    private lateinit var articleDao: ArticleDao

    @BeforeEach
    fun setup() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        baseUrl = mockWebServer.url("")
        articleService = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(ArticleService::class.java)

        articleRemoteSource = ArticleRemoteSource(articleService)
        articleDao = ArticleDaoFake(appDatabaseFake = appDatabase)
        articleRepositoryImp = ArticleRepositoryImp(articleRemoteSource,articleDao)

        fetchAndSaveArticlesUseCase = FetchAndSaveArticlesUseCase(articleRepositoryImp)
    }


    /**
     * Simulate a bad request
     */
    @Test
    fun fetchAndSaveArticlesFromNetwork_returnsError(): Unit = runBlocking {

        // condition the response
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST)
                .setBody("{}")
        )

        // confirm the cache is empty to start
        val articlesFromAsFlow = articleDao.findAll().toList()
        assert(articlesFromAsFlow[0].isEmpty())

        // run use case
        val articlesAsFlow = fetchAndSaveArticlesUseCase.invoke().toList()

        // first emission should be a loading
        assert(articlesAsFlow[0] is Resource.Loading)

        // second emission is the data with the remote data
        assert(articlesAsFlow[1] is Resource.Error)
    }


    /**
     *  Simulate a success
     */
    @Test
    fun fetchAndSaveArticlesFromNetwork_returnList(): Unit = runBlocking {
        // condition the response
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(MockWebServerResponses.listResponse)
        )

        // confirm the cache is empty to start
        val articlesFromAsFlow = articleDao.findAll().toList()
        assert(articlesFromAsFlow[0].isEmpty())

        // run use case
        val articlesAsFlow = fetchAndSaveArticlesUseCase.invoke().toList()

        // first emission should be loading
        assert(articlesAsFlow[0] is Resource.Loading)

        // second emission is the data with the remote data
        assert((articlesAsFlow[1] as Resource.Success).data.isNotEmpty())
    }

    @AfterEach
    fun tearDown() {
        mockWebServer.shutdown()
    }
}