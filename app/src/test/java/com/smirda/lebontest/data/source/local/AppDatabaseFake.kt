package com.smirda.lebontest.data.source.local

import com.smirda.lebontest.domain.model.Article

class AppDatabaseFake {
    // fake for articles table in local db
    val articles = mutableListOf<Article>()
}
