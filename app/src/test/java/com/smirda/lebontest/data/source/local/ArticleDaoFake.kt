package com.smirda.lebontest.data.source.local

import com.smirda.lebontest.domain.model.Article
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


class ArticleDaoFake(
    private val appDatabaseFake: AppDatabaseFake
): ArticleDao {

    override fun insertAll(articles: List<Article>) {
        appDatabaseFake.articles.addAll(articles)
    }

    override fun findAll(): Flow<List<Article>> {
       return flow{ emit(appDatabaseFake.articles)}
    }

    override fun deleteAll() {
        appDatabaseFake.articles.clear()
    }
}
