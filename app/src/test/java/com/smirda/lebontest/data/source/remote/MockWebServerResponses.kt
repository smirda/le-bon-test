package com.smirda.lebontest.data.source.remote

object MockWebServerResponses {

    /**
     * data from api
     */
    const val listResponse: String  = "[\n" +
            "  {\n" +
            "    \"albumId\": 1,\n" +
            "    \"id\": 1,\n" +
            "    \"title\": \"accusamus beatae ad facilis cum similique qui sunt\",\n" +
            "    \"url\": \"https://via.placeholder.com/600/92c952\",\n" +
            "    \"thumbnailUrl\": \"https://via.placeholder.com/150/92c952\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"albumId\": 1,\n" +
            "    \"id\": 2,\n" +
            "    \"title\": \"reprehenderit est deserunt velit ipsam\",\n" +
            "    \"url\": \"https://via.placeholder.com/600/771796\",\n" +
            "    \"thumbnailUrl\": \"https://via.placeholder.com/150/771796\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"albumId\": 1,\n" +
            "    \"id\": 3,\n" +
            "    \"title\": \"officia porro iure quia iusto qui ipsa ut modi\",\n" +
            "    \"url\": \"https://via.placeholder.com/600/24f355\",\n" +
            "    \"thumbnailUrl\": \"https://via.placeholder.com/150/24f355\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"albumId\": 1,\n" +
            "    \"id\": 4,\n" +
            "    \"title\": \"culpa odio esse rerum omnis laboriosam voluptate repudiandae\",\n" +
            "    \"url\": \"https://via.placeholder.com/600/d32776\",\n" +
            "    \"thumbnailUrl\": \"https://via.placeholder.com/150/d32776\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"albumId\": 1,\n" +
            "    \"id\": 5,\n" +
            "    \"title\": \"natus nisi omnis corporis facere molestiae rerum in\",\n" +
            "    \"url\": \"https://via.placeholder.com/600/f66b97\",\n" +
            "    \"thumbnailUrl\": \"https://via.placeholder.com/150/f66b97\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"albumId\": 1,\n" +
            "    \"id\": 6,\n" +
            "    \"title\": \"accusamus ea aliquid et amet sequi nemo\",\n" +
            "    \"url\": \"https://via.placeholder.com/600/56a8c2\",\n" +
            "    \"thumbnailUrl\": \"https://via.placeholder.com/150/56a8c2\"\n" +
            "  }]"
}