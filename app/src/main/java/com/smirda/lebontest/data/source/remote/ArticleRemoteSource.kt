package com.smirda.lebontest.data.source.remote

import com.smirda.lebontest.data.source.remote.service.ArticleService
import com.smirda.lebontest.domain.common.Error
import com.smirda.lebontest.domain.common.Resource
import com.smirda.lebontest.domain.model.Article

class ArticleRemoteSource constructor(private val articleService: ArticleService) {
    suspend fun fetchArticles() : Resource<List<Article>, Error> {
        return try {
            // Let's fetch all data
            val response = articleService.fetchArticles()
            if(response.isSuccessful){
                val articles = response.body()?.map{ Article(thumbnailUrl = it.thumbnailUrl, url = it.url, id = it.id,title = it.title ) } ?: emptyList()
                Resource.Success(articles)
            }else{
                Resource.Error(Error(response.message(),response.code()))
            }
        }catch (e: Exception){
            Resource.Error(Error(e.message.toString(),-1))
        }
    }
}