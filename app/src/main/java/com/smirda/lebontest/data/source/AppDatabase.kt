package com.smirda.lebontest.data.source

import androidx.room.Database
import androidx.room.RoomDatabase
import com.smirda.lebontest.data.source.local.ArticleDao
import com.smirda.lebontest.domain.model.Article

@Database(
    entities = [
        Article::class,
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase(){
    abstract fun articleDao() : ArticleDao
}