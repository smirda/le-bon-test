package com.smirda.lebontest.data.source.remote.service

import com.smirda.lebontest.data.source.remote.dto.ArticleResponse
import retrofit2.Response
import retrofit2.http.GET

interface ArticleService {
    @GET("img/shared/technical-test.json")
    suspend fun fetchArticles() : Response< List<ArticleResponse>>
}