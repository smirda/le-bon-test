package com.smirda.lebontest.data.source.remote.dto

data class ArticleResponse(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)