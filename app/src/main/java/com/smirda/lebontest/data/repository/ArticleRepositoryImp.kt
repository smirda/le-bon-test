package com.smirda.lebontest.data.repository

import com.smirda.lebontest.data.source.local.ArticleDao
import com.smirda.lebontest.data.source.remote.ArticleRemoteSource
import com.smirda.lebontest.domain.common.Resource
import com.smirda.lebontest.domain.model.Article
import com.smirda.lebontest.domain.repository.ArticleRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import com.smirda.lebontest.domain.common.Error

/**
 * This is the implementation of the ArticleRepository
 */
class ArticleRepositoryImp constructor(private val articleRemoteSource: ArticleRemoteSource,private val articleDao: ArticleDao) : ArticleRepository {

    /**
     * Let's retrieve data from network
     */
    override fun fetchAndSaveArticles(): Flow<Resource<List<Article>, Error>> {
        return flow {
            emit(Resource.Loading)
            // Let's fetch and return the result
            val result = articleRemoteSource.fetchArticles()
            //Let's save locally only on success
            if(result is Resource.Success<List<Article>>){
                saveToLocal(result.data)
            }
            // Let's tell the presentation layer for any error/or response
            emit(result)
        }
    }

    /**
     * Let's retrieve from DB
     */
    override fun retrieveArticles(): Flow<List<Article>> {
       return articleDao.findAll()
    }

    /**
     * Saving data to database
     */
    private fun saveToLocal(articles: List<Article>){
        // save stories
        articleDao.deleteAll()
        articleDao.insertAll(articles)
    }

}