package com.smirda.lebontest.domain.repository

import com.smirda.lebontest.domain.common.Resource
import com.smirda.lebontest.domain.common.Error
import com.smirda.lebontest.domain.model.Article
import kotlinx.coroutines.flow.Flow

interface ArticleRepository {
    fun fetchAndSaveArticles() : Flow<Resource<List<Article>, Error>>
    fun retrieveArticles() : Flow<List<Article>>
}