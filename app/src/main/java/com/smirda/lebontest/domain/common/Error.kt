package com.smirda.lebontest.domain.common

data class Error(
    val message: String,
    val code: Int
)