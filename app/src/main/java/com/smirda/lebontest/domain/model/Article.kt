package com.smirda.lebontest.domain.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "article", indices = [Index(value = ["id"], unique = true)])
data class Article(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)