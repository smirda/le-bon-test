package com.smirda.lebontest.domain.common

sealed class Resource <out T :Any, out O:Any> {
    object Loading: Resource<Nothing, Nothing>()
    data class Success<T:Any>(val data : T) : Resource<T, Nothing>()
    data class Error <O : Any>(val error: O) : Resource<Nothing, O>()
}