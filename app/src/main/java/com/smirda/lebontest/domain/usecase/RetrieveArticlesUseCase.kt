package com.smirda.lebontest.domain.usecase

import com.smirda.lebontest.domain.model.Article
import com.smirda.lebontest.domain.repository.ArticleRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RetrieveArticlesUseCase  @Inject constructor(private val articleRepository: ArticleRepository) {
    //with operator no need to call invoke*
    operator fun invoke() : Flow<List<Article>> {
        return articleRepository.retrieveArticles()
    }
}