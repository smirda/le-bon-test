package com.smirda.lebontest.di

import com.smirda.lebontest.data.source.AppDatabase
import com.smirda.lebontest.data.source.local.ArticleDao
import com.smirda.lebontest.data.source.remote.ArticleRemoteSource
import com.smirda.lebontest.data.repository.ArticleRepositoryImp
import com.smirda.lebontest.data.source.remote.service.ArticleService
import com.smirda.lebontest.domain.repository.ArticleRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object ArticlesModule {

    @Provides
    @Singleton
    fun provideArticleRemoteApi(retrofit: Retrofit): ArticleService {
        return retrofit.create(ArticleService::class.java)
    }

    @Provides
    @Singleton
    fun provideArticlesRemoteSource(articlesService: ArticleService): ArticleRemoteSource {
        return ArticleRemoteSource(articlesService)
    }

    @Provides
    @Singleton
    fun provideArticlesRepository(articlesRemoteSource: ArticleRemoteSource, articleDao: ArticleDao) : ArticleRepository {
        return ArticleRepositoryImp(articlesRemoteSource,articleDao)
    }

    @Provides
    @Singleton
    fun provideArticleDao(appDatabase: AppDatabase) : ArticleDao {
        return appDatabase.articleDao()
    }
}