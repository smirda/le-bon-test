package com.smirda.lebontest.presentation.home.ui.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.smirda.lebontest.domain.common.Resource
import com.smirda.lebontest.domain.model.Article
import com.smirda.lebontest.domain.usecase.FetchAndSaveArticlesUseCase
import com.smirda.lebontest.domain.usecase.RetrieveArticlesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel  @Inject constructor(
    private val fetchAndSaveArticlesUseCase: FetchAndSaveArticlesUseCase,
    retrieveArticlesUseCase: RetrieveArticlesUseCase) : ViewModel() {

    val TAG = "MainViewModel"

    val articles : LiveData<List<Article>> =  retrieveArticlesUseCase().asLiveData()


    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading : LiveData<Boolean> get() = _isLoading

    init {
        //Let's fetch data
        viewModelScope.launch {
            fetchAndSaveArticlesUseCase().collect { result ->
                _isLoading.value = false
                when(result){
                    is Resource.Error -> {
                        _isLoading.value = false
                        Log.e(
                            TAG,
                            "An error as occured ${result.error.message} - ${result.error.code}"
                        )
                    }
                    is Resource.Loading -> _isLoading.value = true
                    is Resource.Success -> _isLoading.value = false

                }
            }
        }
    }



}