package com.smirda.lebontest.presentation.home.ui.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.smirda.lebontest.R
import com.smirda.lebontest.databinding.ViewHolderArticleBinding
import com.smirda.lebontest.domain.model.Article

/**
 *
 */
class ArticleAdapter() :
    ListAdapter<Article, ArticleAdapter.ItemViewHolder>(UserDetailsDiffCallback()) {


    inner class ItemViewHolder(private val articleBinding: ViewHolderArticleBinding) :
        RecyclerView.ViewHolder(articleBinding.root) {
        fun bind(item: Article) {
            articleBinding.article = item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: ViewHolderArticleBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.view_holder_article,
            parent, false
        )
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    /**
     * Calculates the difference between two lists and outputs a list of update operations that converts the first list into the second one
     */
    class UserDetailsDiffCallback : DiffUtil.ItemCallback<Article>() {

        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.title == newItem.title
        }


    }

}