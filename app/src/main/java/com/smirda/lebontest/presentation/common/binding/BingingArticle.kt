package com.smirda.lebontest.presentation.common.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.Headers
import com.bumptech.glide.request.RequestOptions
import com.smirda.lebontest.R
import com.smirda.lebontest.domain.model.Article

@BindingAdapter("loadImage")
fun loadImage(view:ImageView, article: Article?){
    article?.let {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
        // We need to add this user agent to get the image from resource via.placeholder
        val headers = Headers { mutableMapOf("User-Agent" to "your-user-agent")}
        val glideUrl = GlideUrl(
            it.url,
            headers
        )
        val glideThumbnail = GlideUrl(
            it.thumbnailUrl,
            headers
        )
        Glide.with(view).load(glideUrl)
            .thumbnail(Glide.with(view.context).load(glideThumbnail))
            .error(R.drawable.ic_baseline_image_24)
            .apply(requestOptions).into(view)
    }
}