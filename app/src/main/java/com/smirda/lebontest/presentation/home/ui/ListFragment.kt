package com.smirda.lebontest.presentation.home.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.smirda.lebontest.R
import com.smirda.lebontest.databinding.ActivityMainBinding
import com.smirda.lebontest.databinding.FragmentListBinding
import com.smirda.lebontest.presentation.home.ui.adapter.ArticleAdapter
import com.smirda.lebontest.presentation.home.ui.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * List [Fragment] subclass.
 */
@AndroidEntryPoint
class ListFragment : Fragment() {

    private val articleAdapter = ArticleAdapter()

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerView()
        observeArticles()
    }

    private fun setRecyclerView() {
        binding.recyclerview.apply {
            adapter = articleAdapter
            // set a GridLayoutManager with 2 number of columns
            layoutManager = GridLayoutManager(context,2)
        }
    }

    /**
     * Here we observe any changes in data
     */
    private fun observeArticles(){
        viewModel.articles.observe(viewLifecycleOwner) {
            articleAdapter.submitList(it)
        }

        viewModel.isLoading.observe(viewLifecycleOwner) {
            binding.idProgressbar.visibility = if (it) View.VISIBLE else View.GONE
            // We can use here skeleton for better ui/ux
        }
    }

}